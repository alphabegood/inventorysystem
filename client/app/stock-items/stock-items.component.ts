import { Component, PipeTransform, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { ProductService } from 'app/_services/product.service';
import { StockItemsService } from 'app/_services/stockitems.service';
import { SearchKeywordPipe } from 'app/_helpers/search-keyword.pipe';


interface Product {
  Id: number;
  Name: string;
  Count: number;
}

interface Alert {
  type: string;
  message: string;
}

@Component({
  selector: 'app-stock-items',
  templateUrl: './stock-items.component.html',
  styleUrls: ['./stock-items.component.css'],
  providers: [DecimalPipe]
})
export class StockItemsComponent implements OnInit {


  products: Product[] = [];
  manualCount: any[] = [];
  filter = new FormControl('');
  alerts: Alert[] = [];

  constructor(pipe: DecimalPipe,
    private productSrv: ProductService,
    private stockItemsSrv: StockItemsService) {
  }

  ngOnInit() {

    this.loadProductList();
  }


  loadProductList() {
    this.productSrv.getList().subscribe((data) => {
      this.products = <Product[]>data;

    }, (error) => {

    });
  }

  overrideCount(productId: number) {

    if (productId && this.manualCount[productId]) {

      let body: object = {
        Amount: this.manualCount[productId],
        Product: { ProductId: productId }
      };

      let currentProduct = <Product>this.products.find(p => p.Id === productId);

      this.stockItemsSrv.override(body).subscribe((data: any) => {

        this.manualCount[productId] = '';

        this.loadProductList();

        let alertMsg: Alert = { type: 'success', message: `The stock of ${currentProduct.Name} has been  overriden with ${data.length} items.` };

        this.alertBuilder(alertMsg);

      }, (error: any) => {

        this.alertBuilder(error);
        this.manualCount[productId] = '';

      });
    }
  }

  addOneItem(productId: number) {

    if (productId) {

      let body: object = {
        ProductId: productId
      };

      this.stockItemsSrv.add(body).subscribe(() => {

        this.manualCount[productId] = '';

        this.loadProductList();
      }, (error) => {

        this.alertBuilder(error);

      });
    }
  }

  removeOneItem(productId: number) {

    if (productId) {

      let body: object = {
        Amount: 1, Product: {
          ProductId: productId
        }
      };

      this.stockItemsSrv.remove(body).subscribe(() => {

        this.manualCount[productId] = '';

        this.loadProductList();

      }, (error) => {
        this.alertBuilder(error);
      });
    }
  }


  addProduct(product: Product){

    this.productSrv.create(product).subscribe((data: any)=>{

      debugger
      let alertMsg : Alert;
      
      if(data.affectedRows){
      
        alertMsg = { type: 'success', message: `${product.Name} added successfully.` };

        this.alertBuilder(alertMsg);

        this.loadProductList();
      }
      else{
        alertMsg = {type: 'danger', message : JSON.stringify(data)};
      }
      

    }), (error: any) => {
      this.alertBuilder(error);
    }
  }

  removeProduct(product: Product){

    this.productSrv.remove(product.Id).subscribe((data: any)=>{

      let alertMsg : Alert;
      
        alertMsg = { type: 'success', message: `${product.Name} removed successfully.` };

        this.alertBuilder(alertMsg);

        this.loadProductList();
      

    }), (error: any) => {
      this.alertBuilder(error);
    }
  }



  close(alert: Alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  reset() {
    this.alerts = [];
  }


  /**
   * Generate the Alert message with a timer
   * @param response 
   */
  alertBuilder(response: any, timer: number = 5000) {


    let alertMsg: Alert;

    if (response.type && response.message) {

      alertMsg = response;
    }
    else {

      //Exploit the error message
      //Cast to error to reuseable object
      let responseObject = <any>response;

      console.log("ALERT ",responseObject);
      if (responseObject && responseObject.error && responseObject.error.error) {
        responseObject.error.error.type = 'danger';
        alertMsg = responseObject.error.error;
      }
      else if (responseObject && responseObject.error && responseObject.error.code) {
        responseObject.error.code.type = 'danger';
        responseObject.error.code.message = responseObject.error.code.sqlMessage;
        alertMsg = responseObject.error.code;
      }
      else {
        alertMsg = { type: 'danger', message: response };
      }
    }

    this.alerts.push(alertMsg);

    //Alert will be closed after it times out
    setTimeout(() => {
      this.close(alertMsg);
    }, timer);
  }


}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StockItemsComponent } from './stock-items/stock-items.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchKeywordPipe } from './_helpers/search-keyword.pipe';
import { AddProductComponent } from './add-product/add-product.component';
import { SurfRecorderComponent } from './surf-recorder/surf-recorder.component';

@NgModule({
  declarations: [
    AppComponent,
    StockItemsComponent,
    SearchKeywordPipe,
    AddProductComponent,
    SurfRecorderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    AppRoutingModule
  ],
  providers: [SearchKeywordPipe],
  exports: [StockItemsComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }


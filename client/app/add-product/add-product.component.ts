import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {


  @Output() createProduct = new EventEmitter();
  productForm = this.formBuilder.group({
    Name: ['', Validators.required]
  });
  

  constructor(private modalService: NgbModal, private formBuilder: FormBuilder) {

  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    });
  }

  //Emit the product payload to the parent component (stockitems)
  onSubmit() {
    if(!this.productForm.valid){
      return;
    }
    this.createProduct.emit(this.productForm.value);
    this.productForm.reset;
    this.modalService.dismissAll();
    
  }

  ngOnInit() {
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
 
@Injectable({
  providedIn: 'root'
})
export class StockItemsService {

  constructor(private http: HttpClient) { }


  override(body: any){
    return this.http.post(`${environment.apiUrl}/overridestockitems`, body , httpOptions);
  }

  add(body: any){
    return this.http.post(`${environment.apiUrl}/stockitem`, body , httpOptions);
  }

  remove(body: any){
    return this.http.post(`${environment.apiUrl}/removestockitems`, body,  httpOptions);
  }

}

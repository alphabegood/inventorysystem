import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
 
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }


  getList(){
    return this.http.get(`${environment.apiUrl}/products`, httpOptions);
  }

  create(body: any){
    return this.http.post(`${environment.apiUrl}/product`, body, httpOptions);
  }

  remove(productId: any){
    return this.http.delete(`${environment.apiUrl}/product/${productId}`, httpOptions);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurfRecorderComponent } from './surf-recorder.component';

describe('SurfRecorderComponent', () => {
  let component: SurfRecorderComponent;
  let fixture: ComponentFixture<SurfRecorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurfRecorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurfRecorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, OnChanges } from '@angular/core';


var startTime, records = [];

@Component({
  selector: 'app-surf-recorder',
  templateUrl: './surf-recorder.component.html',
  styleUrls: ['./surf-recorder.component.css']
})
export class SurfRecorderComponent implements OnInit {

  eventSaved = [];
  RECORD: string = 'local_see';
  REPLAY: string = 'play_circle_filled';
  PAUSE: string = 'local_see';
  PAUSEREPLAY: string = 'pause_circle_filled';
  recordButton: string;
  playButton: string;
  eventTypes: string = "click";
  numberRecords: number;
  capturing: boolean = false;

  constructor() { }

  ngOnInit() {
    this.recordButton = this.RECORD;
    this.playButton = this.REPLAY;
    this.numberRecords = records.length;
  }





  startRecording() {

    this.capturing = true;

    startTime = (new Date()).getTime();

    console.log(startTime);

    document.body.addEventListener(this.eventTypes, this.saveEvent);


  }

  saveEvent(event) {

    let offsetTime = ((new Date()).getTime() - startTime);

    console.log(startTime, { time: offsetTime, event: event });

    records.push({ time: offsetTime, event: event });
    //console.log(this.eventSaved.length);
  }

  pauseRecorder(event) {

    //event.stopPropagation();

    this.capturing = false;

    document.body.removeEventListener(this.eventTypes, this.saveEvent);
    this.numberRecords = records.length;
    console.log("PAUSE");
  }



  recorderAction(event) {
    if (!this.capturing) {
      this.startRecording();
    }
    else {
      this.pauseRecorder(event);
    }
  }


  replayEvents() {

    this.playButton = this.PAUSEREPLAY;

    let previousTime = 0;
    for (let index = 0; index < this.numberRecords; index++) {
      let eventElem: any = records[index];

      let currentTimer = eventElem.time - previousTime;

      setTimeout(() => {

        console.log(eventElem);

        previousTime = eventElem.time;

        let mouseEvent = document.createEvent("MouseEvent");

        let { type, canBubble, cancelable, view,
          detail, screenX, screenY, clientX, clientY,
          ctrlKey, altKey, shiftKey, metaKey,
          button, relatedTarget } = eventElem.event;

        console.log(type, canBubble, cancelable, view,
          detail, screenX, screenY, clientX, clientY,
          ctrlKey, altKey, shiftKey, metaKey,
          button, relatedTarget);

        mouseEvent.initMouseEvent(type, canBubble, cancelable, view,
          detail, screenX, screenY, clientX, clientY,
          ctrlKey, altKey, shiftKey, metaKey,
          button, relatedTarget);

        document.body.dispatchEvent(mouseEvent);

        if (index == this.numberRecords - 1) {
          console.log("Finish");
        }
      }, currentTimer);

    }



  }

  pauseEvents() {

    this.playButton = this.REPLAY;

  }

  replayAction() {

    if (records.length > 0) {
      if (this.playButton === this.REPLAY) {
        this.replayEvents();
      }
      else {
        this.pauseEvents();
      }
    }

  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchKeyword'
})
export class SearchKeywordPipe implements PipeTransform {

  transform(values: any[], searchKeyword: string): any {
    if(!searchKeyword || searchKeyword === '' ){
      return values;
    }
    return values.filter((v) => {return v['Name'].toLowerCase().includes(searchKeyword.toLowerCase());});
  }

}

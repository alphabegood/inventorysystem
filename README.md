#Inventory web application showing accurate number of items in stock

##Introduction

This inventory system, built with Angular (v. 8) NodeJs(v. 10.16.0), Express (v. 4) the MySQL database, allows to manage easily a warehouse. It helps to enter a product, add/remove item to/from the stock.   

##Prerequisites

You should have installed on your development or production environment:
NodeJs (Version 10.16.0 or above)
Mysql Server (Version 5.6)

![Alt text](./demoAnimation.gif "Demo Animation")

The full recorded demo is available on this [link](https://drive.google.com/file/d/1cjPyyw1WUf1ws-Ky-fQxzzO8KBIHV0xQ/view?usp=sharing).

##Developement environment

Install all dependencies. See package.json for all dependencies details.
`npm install`

Install TypeScript execution and REPL (virtual environment) for node.js
`npm install -g ts-node` 
`npm install -g typescript`

Install nodemon to run the backend in development mode 
`npm install -g nodemon`

Install the angular CLI which helps to manage your UI project 
`npm install -g @angular/cli`

Install concurrently to run simultaneously several command instructions from one (Back-end and Front-end)
`npm install -g concurrently`

Then install process manager to run NodeJs server for production environment
```npm install pm2 -g```

##Database setup

The file `api/init.database.sql` contains the SQL queries to instantiate the Warehouse database, its two tables and the relationship between both. It inserts as well some examples. Make sure to update ```.env.dev``` file about your database settings.

##Start the local server:

Run `npm run dev` to restart the front-end and the back-end.
The local environment is using ```.env.dev``` config file for the backend and  ```client/environments/environment.ts``` which is for the API host. To configure the API host for the production environment the file is ```client/environments/environment.prod.ts```.

##Tests

*Postman*: Import the file ```postmanCollection.json``` into Postman for the API collection test. This is for a manual test. No assertion is added to the requests.

*Chai assertion testing*: The command ```npm run apitest``` runs the test cases provided in ```api/test/product.spec.ts``` file.  



##Get ready the production deployment code (local folder)

Run the command below to get the UI and API code ready for deployment. This transpiles and copies the code into the ```dist``` folder. Also, the ```node_modules``` folder and ```.env.prod``` production config file will be copied over.  
```npm run build```

##Run Production server
Go the the dist folder generated from the previous build command and run:
```NODE_ENV=production node api/server.js``` 
or 

```pm2 start ecosystem.config.js --env production``` which is using ```ecosystem.config.js``` file which can contains the configuration settings.







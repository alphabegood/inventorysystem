module.exports = {
  apps : [
      {
        name: "AlphaInventorySystem",
        script: "./api/server.js",
        watch: false,
        env: {
            "PORT": 3000,
            "NODE_ENV": "development"
        },
        env_production: {
            "PORT": 80,
            "NODE_ENV": "production",
        }
      }
  ]
}

// Controllers (route handlers)
import express from "express";
import * as path from 'path';
import * as productController from "./controllers/productController";
import * as stockItemController from "./controllers/stockItemController";


export default function setRoutes(app: any) {

    const router = express.Router();

    //Routes definition

    app.use('/', express.static(path.join(__dirname, '../client')));
    //Default route for production
    // Redirect / to our html file
    router.route('/').get((req, res) =>  res.sendFile(path.resolve('../client/index.html')));

    //Read all products with their number of items in stock 
    router.route("/products").get(productController.list);
    //Read a product label 
    router.route("/product/:id").get(productController.read);
    //Create a product label 
    router.route("/product").post(productController.create);
    //Update a product label 
    router.route("/product/:id").put(productController.update);
    //Delete a product label 
    router.route("/product/:id").delete(productController.remove);

    //Read a stock item  
    router.route("/stockitem/:id").get(stockItemController.read);
    //Create stock items (in bulk)
    router.route("/addstockitems").post(stockItemController.createBulk);
    //Overriden stock items
    router.route("/overridestockitems").post(stockItemController.override);
    //Create a stock item
    router.route("/stockitem").post(stockItemController.create);
    //Update a stock item 
    router.route("/stockitem/:id").put(stockItemController.update);
    //Delete a stock item 
    router.route("/stockitem/:id").delete(stockItemController.remove);
    //Remove stock items (in bulk) 
    router.route("/removestockitems").post(stockItemController.removeBulk);

    //Apply prefix "api" to all routes associated with our Express server instance
    app.use('/api', router);

}
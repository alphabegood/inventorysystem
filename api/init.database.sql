#--Warehouse Database creation
CREATE DATABASE IF NOT EXISTS `Warehouse`;


#--Product Table creation
CREATE TABLE `Warehouse`.`Product` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC));

#--Products items to add to Product Table
INSERT INTO `Warehouse`.`Product` (`Name`) VALUES ('Doxycycline Hyclate');
INSERT INTO `Warehouse`.`Product` (`Name`) VALUES ('Lipitor');
INSERT INTO `Warehouse`.`Product` (`Name`) VALUES ('Prednisone');


#--Stock Table creation
CREATE TABLE `Warehouse`.`Stock` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `ProductId` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `ProductRef_idx` (`ProductId` ASC),
  CONSTRAINT `ProductRef`
    FOREIGN KEY (`ProductId`)
    REFERENCES `Warehouse`.`Product` (`Id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);


#--Stocks to add to Stock Table
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('1');
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('1');
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('1');
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('2');
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('2');
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('3');
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('3');
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('3');
INSERT INTO `Warehouse`.`Stock` (`ProductId`) VALUES ('1');

#--Delete a product item to check the constraint key relationships (Test)
DELETE FROM `Warehouse`.`Product` WHERE (`Id` = '3');


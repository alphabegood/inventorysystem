//set development env forthe test
process.env.NODE_ENV = 'development';


import server from '../server';
import * as chai from 'chai';
import chaiHttp = require('chai-http');
import { Product, ProductModel } from "../models/productModel";

chai.use(chaiHttp);
import 'mocha';

//Parse the assertion library to get the request function as chai.request is not found
let chaiLib = <any>chai;
let chaiRequestLib = chaiLib.default.request;

//Initiate the 
const expect = chai.expect;
const should = chai.should();
let productTest: any = undefined;


//Initiate the Test by removing the product test "Dexeryl Cream 250g"
describe('Delete product "Dexeryl Cream 250g"', () => {

  it('should delete product "Dexeryl Cream 250g"', () => {

    let product = <Product>{ Name: "Dexeryl Cream 250g" };
    let productModel = new ProductModel(product);

    (async () => {
      let result: any = await productModel.remove();
      chai.assert.isObject(result);
      chai.assert.equal(result.affectedRows, 1 , '"Dexeryl Cream 250g" not delete.');
    })();


  })
});

//Create  "Dexeryl Cream 250g" product test
describe('Create product "Dexeryl Cream 250g"', () => {

  it('should create product "Dexeryl Cream 250g"', () => {

    let product = { Name: "Dexeryl Cream 250g" };

    return chaiRequestLib(server).post('/api/product')
    .send(product)
    .then((res: any) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      chai.assert.equal(res.body.affectedRows, 1 , '"Dexeryl Cream 250g" product not created');
    })

  })
});




//List products
describe('Check list of products', () => {

  it('should return a list of products', () => {
    
    return chaiRequestLib(server).get('/api/products')
      .then((res: any) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        chai.assert(Array.isArray(res.body), 'The response is not a list (array) of products');
      })
  })
});

//Check "Dexeryl Cream 250g" within the list of products
describe('Check "Dexeryl Cream 250g" within the list of products', () => {

  it('should find "Dexeryl Cream 250g" without item in stock', () => {
    
    return chaiRequestLib(server).get('/api/products')
      .then((res: any) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        productTest = res.body.find((p: any)=>{return p.Name == 'Dexeryl Cream 250g'});

        chai.assert.isObject(productTest, '"Dexeryl Cream 250g" is not listed');
        chai.assert.equal(productTest.Count, 0, 'There is no item for "Dexeryl Cream 250g"');
      })
  })
});


//Add "Dexeryl Cream 250g" items in stock 
describe('Add "Dexeryl Cream 250g" an item in stock', () => {

  it('should add "Dexeryl Cream 250g" an item in stock', () => {
    
    chai.assert.isAbove(productTest.Id, 0, 'The "Dexeryl Cream 250g" does not have an Id');

    let stockItem = {ProductId: productTest.Id};

    return chaiRequestLib(server).post('/api/stockitem')
    .send(stockItem)
    .then((res: any) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      chai.assert.equal(res.body.affectedRows, 1 , '"Dexeryl Cream 250g" item is not added in stock');
    })

  })
});
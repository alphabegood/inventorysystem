import express from "express";
import compression from "compression";
import session from "express-session";
import bodyParser from "body-parser";
import dotenv from "dotenv"; 8
import morgan from "morgan";
import databaseWrapper from "./databaseWrapper";
import cors from "cors";


//Express server instantiated
const app = express();


//Developement config
if (process.env.NODE_ENV !== 'production') {
    // Load environment variables
    dotenv.config({ path: ".env.dev" });
    app.use(cors());

    var allowCrossDomain = function (req: express.Request, res: express.Response, next: any) {
        res.header('Access-Control-Allow-Origin', 'example.com');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type');

        next();
    }
}
//PRODUCTION config
else{
    dotenv.config({ path: ".env.prod" });
}



// Express configuration
app.set("port", process.env.PORT);
app.use(compression({ level: -1 })); //default compression
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: process.env.SECRET_SESSION
}));


//HTTP request logger implemented on the server instance
app.use(morgan('dev'))

//Mysql connection instantiated
// const databaseWrapper = require("./databaseWrapper");

databaseWrapper.connection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    connectionLimit: process.env.DB_CONNECTION_LIMIT,
    insecureAuth: true
}).then((pool: any) => {
    console.log("Database connection successful");
},
    (error: any) => {
        console.error("Database connection error: ", error);
    });



export default app;

import mysql from "mysql";

/**
 * Wrap the asynchronous mysql function to work comfortably with promise instead of callback function  
 */
var pool: any;

export default {

    connection: function (config: object) {

        pool = mysql.createPool(config);

        function getConnection() {

            return new Promise((resolve, reject) => {

                try {

                    pool.getConnection((error: any, connection: any) => {

                        if (error) {
                            return reject(error);
                        }

                        if (connection) {
                            connection.release();
                        }

                        resolve(pool);
                    });

                }
                catch (exceptionError) {
                    return reject(exceptionError);
                }

            });
        }

        return getConnection();

    },
    query: function (sql: string, args: any) {

        return new Promise((resolve, reject) => {

            try {
                let qry = pool.query(sql, args, (queryError: any, rows: any) => {

                    if (queryError) {
                        return reject(queryError);
                    }
                    resolve(rows);
                });
                //Debug query
                //console.log(qry);
            }
            catch (exceptionError) {
                return reject(exceptionError);
            }

        });
    }

}




    // async function close() {

    //     return new Promise((resolve, reject) => {

    //         connection.end((err: any) => {

    //             if (err) {
    //                 return reject(err);
    //             }

    //             resolve();
    //         });
    //     });
    // }

import app from "./app";
import routes from "./routes";



//Call routes definition
routes(app);


/**
 * Start Express server.
 */
const server = app.listen(app.get("port"), () => {

  console.log(
    "Web Server running at http://localhost:%d in %s mode",
    app.get("port"),
    app.get("env")
  );
  console.log("To stop the server press Ctrl+c keys simultaneously.\n");
});

export default server;
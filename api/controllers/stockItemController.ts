'use strict';

import { Request, Response } from "express";
import { StockItem, StockItemModel } from "../models/stockItemModel";

/**
 * create stock item used in routes definition
 * @param req 
 * @param res 
 */
export let create = async (req: Request, res: Response) => {

    try {

        //Cast the request parameters to Stock item object
        let data = <StockItem>req.body;
        //Instantiate the model class
        let stockItemModel = new StockItemModel(data);
        //as we its can create the stock item into the table database
        let result: any = await stockItemModel.create();
        res.send(result);

    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }

};


/**
 * create stock items (bulk) used in routes definition
 * @param req 
 * @param res 
 */
export let createBulk = async (req: Request, res: Response) => {

    try {

        //Cast the request parameters to Stock item object
        let data = req.body;

        if (data.Amount && data.Product) {
            //Instantiate the model class
            let stockItemModel = new StockItemModel(data.Product);

            let results: any[] = [];

            for (let i = 0; i < data.Amount; i++) {
                //as we its can create the stock item into the table database
                results.push(await stockItemModel.create());
            }

            res.send(results);
        }
        else {
            throw { message: "Amount and/or Product payload are not defined." };
        }

    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }

};



/**
 * Override number of stock items used in routes definition
 * @param req 
 * @param res 
 */
export let override = async (req: Request, res: Response) => {

    try {
        //Cast the request parameters to Stock item object
        let data = req.body;

        if (data.Amount && data.Product) {

            //Instantiate the model class
            let stockItemModel = new StockItemModel(data.Product);

            let countItems: any = await stockItemModel.countProductItems();

            //Allow overriding only if there already an amount of items

            if(!Array.isArray(countItems)){
                throw { message: `Something went wrong please contact the Administrator.` }; 
            }

            if(countItems && countItems[0] && countItems[0].CurrentCount === 0){
                throw { message: `The product does not have items in stock to be overriden.` };
            }

            //Check if amount different
            if(countItems[0].CurrentCount === data.Amount){
                throw { message: `The product has already ${countItems[0].CurrentCount} items in stock.` };
            }

            //as we its can trigger removeOne method to delete only one matched product item 
            let resultRemoveAll: any = await stockItemModel.removeAll();

            if (resultRemoveAll.affectedRows) {

                let results: any[] = [];

                for (let i = 0; i < data.Amount; i++) {
                    //as we its can create the stock item into the table database
                    results.push(await stockItemModel.create());
                }

                res.send(results);
            }
            else {
                throw { message: `No items removed for Product Id ${data.Product.ProductId}.` };
            }

        }
        else {
            throw { message: "Amount and/or Product payload are not defined." };
        }

    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }

};

/**
 * Read stock item used in routes definition
 * @param req 
 * @param res 
 */
export let read = async (req: Request, res: Response) => {

    try {

        //pick up the id from url parameter
        let data = <StockItem>{ Id: req.params.id };
        //Instantiate the model class
        let stockItemModel = new StockItemModel(data);
        //as we its can trigger read method to get the entire item 
        let result: any = await stockItemModel.read();

        if (Array.isArray(result) && result.length === 1) {
            res.send(result[0]);
        }
        else {
            throw { message: `Stock item Id ${req.params.id} is not found.` };
        }


    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }



};


/**
 * Read stock item used in routes definition
 * @param req 
 * @param res 
 */
export let update = async (req: Request, res: Response) => {

    try {

        //Cast the request parameters to Product object
        let data = <StockItem>req.body;
        //Instantiate the model class
        let stockItemModel = new StockItemModel(data);
        //as we its can read method to get the entire stock item 
        let result: any = await stockItemModel.update(req.params.id);

        if (result.affectedRows) {
            res.send({ message: `Stock item Id ${req.params.id} sucessfully updated.` });
        }
        else {
            throw { message: `Stock item Id ${req.params.id} is not found.` };
        }


    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }



};


/**
 * Remove stock item used in routes definition
 * @param req 
 * @param res 
 */
export let remove = async (req: Request, res: Response) => {

    try {

        //pick up the id from url parameter
        let data = <StockItem>{ Id: req.params.id };
        //Instantiate the model object
        let stockItemModel = new StockItemModel(data);
        //as we its can trigger remove method to delete the entire stock item 
        let result: any = await stockItemModel.remove();

        if (result.affectedRows) {
            res.send({ message: `Stock item Id ${req.params.id} sucessfully removed.` });
        }
        else {
            throw { message: `Stock item Id ${req.params.id} is not found.` };
        }


    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }

};



/**
 * Remove stock items (bulk) used in routes definition
 * @param req 
 * @param res 
 */
export let removeBulk = async (req: Request, res: Response) => {

    try {


        let data = req.body;

        if (data.Amount && data.Product) {

            //Instantiate the model class
            let stockItemModel = new StockItemModel(data.Product);

            let removedItems: number = 0;
            let failedItems: number = 0;

            for (let i = 0; i < data.Amount; i++) {
                //as we its can trigger removeOne method to delete only one matched product item 
                let result: any = await stockItemModel.removeOne();

                if (result.affectedRows) {
                    removedItems++;
                }
                else {
                    throw { message: `Failed to remove item.` };
                }

            }

            res.send({ message: `${removedItems} items in stock successfully removed.` });

        }
        else {
            throw { message: "Amount and/or Product payload are not defined." };
        }



    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }

};


'use strict';

import { Request, Response } from "express";
import { Product, ProductModel } from "../models/productModel";

/**
 * create product used in routes definition
 * @param req 
 * @param res 
 */
export let create = async (req: Request, res: Response) => {

    try {

        //Cast the request parameters to Product object
        let data = <Product>req.body;
        //Instantiate the model class
        let productModel = new ProductModel(data);
        //as we its can create the product into the table database
        let result: any = await productModel.create();
        res.send(result);

    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }



};


/**
 * Read products used in routes definition
 * @param req 
 * @param res 
 */
export let list = async (req: Request, res: Response) => {

    try {


        //Instantiate the model class
        let productModel = new ProductModel();
        //as we its can trigger read method to get the entire product 
        let result: any  = await productModel.list();

        if (Array.isArray(result) && result.length > 0) {
            res.send(result);
        }
        else {
            throw { message: `Product not found.` };
        }


    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }



};

/**
 * Read product used in routes definition
 * @param req 
 * @param res 
 */
export let read = async (req: Request, res: Response) => {

    try {

        //pick up the id from url parameter
        let data = <Product>{ Id: req.params.id };
        //Instantiate the model class
        let productModel = new ProductModel(data);
        //as we its can trigger read method to get the entire product 
        let result: any  = await productModel.read();

        if (Array.isArray(result) && result.length === 1) {
            res.send(result[0]);
        }
        else {
            throw { message: `Product Id ${req.params.id} is not found.` };
        }


    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }



};


/**
 * Read product used in routes definition
 * @param req 
 * @param res 
 */
export let update = async (req: Request, res: Response) => {

    try {

        //Cast the request parameters to Product object
        let data = <Product>req.body;
        //Instantiate the model class
        let productModel = new ProductModel(data);
        //as we its can trigger read method to get the entire product 
        let result: any = await productModel.update(req.params.id);

        if (result.affectedRows) {
            res.send({message:`Product Id ${req.params.id} sucessfully updated.`});
        }
        else {
            throw { message: `Product Id ${req.params.id} is not found.` };
        }


    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }



};


/**
 * Remove product used in routes definition
 * @param req 
 * @param res 
 */
export let remove = async (req: Request, res: Response) => {

    try {

        //pick up the id from url parameter
        let data = <Product>{ Id: req.params.id };
        //Instantiate the model object
        let productModel = new ProductModel(data);
        //as we its can trigger remove method to delete the entire product 
        let result: any = await productModel.remove();

        if (result.affectedRows) {
            res.send({message:`Product Id ${req.params.id} sucessfully removed.`});
        }
        else {
            throw { message: `Product Id ${req.params.id} is not found.` };
        }


    }
    catch (exceptionError) {
        //In case something goes wrong during the process
        //Defaulted to bad request status but should be determined from the error type 
        res.status(400).send({ error: exceptionError });
    }

};


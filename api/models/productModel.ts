'use strict';

import databaseWrapper from "../databaseWrapper";

//Product properties interface used to map product definition 
type Product = { Id?: number, Name?: string };

/**
 * Helps to communicate with the Product table
 */
class ProductModel {

    private product: Product | undefined;
    private mainFields: any[] = ['Id', 'Name'];
    private tableName: string = 'Product';
    private stockTable: string = 'Stock';

    constructor(product?: Product ) {

        this.product = product;

    }

    /**
     * Query to create product label in Product table
     */
    async create() {

        let query = `INSERT INTO ${process.env.DB_NAME}.${this.tableName} SET ?`;

        return databaseWrapper.query(query, this.product);
    }


    /**
     * Query to read all products and their number items from Product and Stock tables
     */
    async list() {

        let fullFieldNames = this.getFullFieldNames();

        let query = `SELECT ${(fullFieldNames).join(',')}, 
        COUNT(${process.env.DB_NAME}.${this.stockTable}.Id) as Count
        FROM ${process.env.DB_NAME}.${this.tableName}
        LEFT JOIN ${process.env.DB_NAME}.${this.stockTable} 
        ON  ${process.env.DB_NAME}.${this.tableName}.Id = ${process.env.DB_NAME}.${this.stockTable}.ProductId
        GROUP BY ${process.env.DB_NAME}.${this.tableName}.Id 
        ORDER BY ${process.env.DB_NAME}.${this.tableName}.Name ASC`;

        return databaseWrapper.query(query, []);
    }


    /**
     * Query to read product label from Product table
     */
    async read() {

        let query = `SELECT ${(this.mainFields).join(',')} FROM ${process.env.DB_NAME}.${this.tableName} WHERE ?`;

        return databaseWrapper.query(query, this.product);
    }

     /**
     * Query to update product label in Product table
     */
    async update(productId: number) {

        let query = `UPDATE ${process.env.DB_NAME}.${this.tableName} SET ? WHERE Id = ?`;

        return databaseWrapper.query(query, [this.product, productId]);
    }

     /**
     * Query to remove product label in Product table
     */
    async remove() {

        let query = `DELETE FROM  ${process.env.DB_NAME}.${this.tableName} WHERE ?`;

        return databaseWrapper.query(query, this.product);
    }

    /**
     * Transform fields to full name associated with the table 
     */
    getFullFieldNames(){

        return this.mainFields.map(field => {
            return `${process.env.DB_NAME}.${this.tableName}.${field}`;
        });
    }


}

export { Product, ProductModel }
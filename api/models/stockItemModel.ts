'use strict';

import databaseWrapper from "../databaseWrapper";

//Stock item properties interface used to map item definition 
type StockItem = { Id?: number, ProductId?: number };

/**
 * Helps to communicate with the StockItem table
 */
class StockItemModel {

    private stockItem: StockItem | undefined;
    private mainFields: any[] = ['Id', 'ProductId'];
    private tableName: string = 'Stock';

    constructor(stockItem?: StockItem) {

        this.stockItem = stockItem;

    }


    /**
     * Query to create an item in stock table
     */
    async create() {

        let query = `INSERT INTO ${process.env.DB_NAME}.${this.tableName} SET ?`;

        return databaseWrapper.query(query, this.stockItem);
    }

    /**
     * Query to read an item from stock table
     */
    async read() {

        let query = `SELECT ${this.mainFields.join(',')} FROM ${process.env.DB_NAME}.${this.tableName} WHERE ?`;

        return databaseWrapper.query(query, this.stockItem);
    }

    /**
     * Query to retrieve number items of a product from stock table
     */
    async countProductItems() {

        let query = `SELECT COUNT(*) as CurrentCount FROM ${process.env.DB_NAME}.${this.tableName} WHERE ?`;

        return databaseWrapper.query(query, this.stockItem);
    }


    /**
    * Query to update an item in stock table
    */
    async update(itemId: number) {

        let query = `UPDATE ${process.env.DB_NAME}.${this.tableName} SET ? WHERE Id = ?`;

        return databaseWrapper.query(query, [this.stockItem, itemId]);
    }

    /**
    * Query to remove an item from stock table
    */
    async remove() {

        let query = `DELETE FROM  ${process.env.DB_NAME}.${this.tableName} WHERE ?`;

        return databaseWrapper.query(query, this.stockItem);
    }


    /**
    * Query to remove only one item of a product from stock table
    */
    async removeOne() {

        let query = `DELETE FROM  ${process.env.DB_NAME}.${this.tableName} WHERE ? LIMIT 1`;

        return databaseWrapper.query(query, this.stockItem);
    }

    /**
       * Query to remove all items of a procdut from stock table
       */
    async removeAll() {

        let query = `DELETE FROM  ${process.env.DB_NAME}.${this.tableName} WHERE ?`;

        return databaseWrapper.query(query, this.stockItem);
    }


}

export { StockItem, StockItemModel }